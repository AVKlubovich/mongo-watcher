#include "pre.hpp"

#include <mongo/watcher.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/exception/exception.hpp>
#include <mongocxx/exception/authentication_exception.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <bsoncxx/json.hpp>

#include <unistd.h>

#include <iostream>

//
// Формат MongoDB oplog
// https://www.kchodorow.com/blog/2010/10/12/replication-internals/
//

namespace mongo
{
    //{
    //    "ts" :
    //    {
    //        "t" : 1286821977000,
    //        "i" : 1
    //    },
    //    "h" : NumberLong("1722870850266333201"),
    //    "op" : "i",
    //    "ns" : "test.foo",
    //    "o" :
    //    {
    //        "_id" : ObjectId("4cb35859007cc1f4f9f7f85d"),
    //        "x" : 1
    //    }
    //}

    watcher::watcher() noexcept
    {}

    watcher::watcher(config_t cfg)
        :   _cfg(std::move(cfg))
    {}

    watcher::~watcher()
    {}

    int watcher::attach(int operations, const std::string& collection, callback_t callback)
    {
        std::lock_guard<rmutex_t> guard(_mtx_modify);

        const int evtid = ++_evtid_gen;

        if( operations & on_insert )
            attach(evtid, callback, "i", collection);

        if( operations & on_update )
            attach(evtid, callback, "u", collection);

        if( operations & on_remove )
            attach(evtid, callback, "d", collection);

        if( operations & on_noop )
            attach(evtid, callback, "n", collection);

        return evtid;
    }

    void watcher::attach(int evtid, callback_t cb, const std::string& op, const std::string& c)
    {
        auto it = _callbacks.emplace(std::make_pair(op, c), callbacks_t::mapped_type());
        callbacks_t::mapped_type& cbs = it.first->second;
        cbs.emplace(evtid, cb);
    }

    int watcher::detach(int evtid, int operations, const std::string& collection)
    {
        std::lock_guard<rmutex_t> guard(_mtx_modify);

        int count = 0;

        if( operations & on_insert )
            count += detach(evtid, "i", collection);

        if( operations & on_update )
            count += detach(evtid, "u", collection);

        if( operations & on_remove )
            count += detach(evtid, "d", collection);

        if( operations & on_noop )
            count += detach(evtid, "n", collection);

        return count;
    }

    int watcher::detach(int evtid, const std::string& op, const std::string& c)
    {
        auto it = _callbacks.find(std::make_pair(op, c));
        if( it == _callbacks.end() )
            return 0;

        callbacks_t::mapped_type& cbs = it->second;
        const int count = cbs.erase(evtid);

        if( cbs.empty() )
            _callbacks.erase(it);

        return count;
    }

    static const std::string OPLOG = "oplog.rs";
/*
    static bsoncxx::types::b_timestamp last_oplog_ts(mongocxx::database& db)
    {
        bsoncxx::types::b_timestamp last;

        try
        {
            bsoncxx::builder::stream::document order_builder;
            order_builder << "$natural" << -1;

            bsoncxx::builder::stream::document proj;
            proj << "ts" << 1 << bsoncxx::builder::stream::finalize;

            mongocxx::options::find opts;
            opts.sort(order_builder.view());
            opts.projection(proj.view());
            opts.limit(1);

            auto cursor = db[OPLOG].find({}, opts);

            for( auto&& doc : cursor )
            {
                last = doc["ts"].get_timestamp();
                break;
            }
        }
        catch(const std::exception &e )
        {
            std::string msg = "cannot read last timestamp: ";
            msg += e.what();
            throw std::runtime_error(msg);
        }

        return last;

    }
*/

    static bsoncxx::types::b_timestamp now_ts()
    {
        bsoncxx::types::b_timestamp last;
        last.timestamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        last.increment = 0;
        return last;
    }

    void watcher::run(bool all)
    {
        {
            std::lock_guard<mutex_t> guard(_mtx_stop);
            _running = true;
        }

        while(_running)
        {
            try
            {
                _run(all);
            }
            catch(const std::exception& e)
            {
                (void)e;
            }
        }
    }

    void watcher::_run(bool all)
    {
        mongocxx::uri uri(_cfg._uri.empty() ? mongocxx::uri::k_default_uri : _cfg._uri);
        mongocxx::pool pool(uri);
        mongocxx::pool::entry client = pool.acquire();
        mongocxx::database db = client->database(_cfg._db);
        if( !db.has_collection(OPLOG) )
            throw std::invalid_argument("bad database name");

        mongocxx::collection coll = db[OPLOG];

        bsoncxx::types::b_timestamp start_ts = {0,0};
        bsoncxx::builder::stream::document filter;
        if( !all )
        {
            start_ts = now_ts();
            filter << "ts"
                << bsoncxx::builder::stream::open_document
                    << "$gt" << start_ts
                << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize;
        }

        mongocxx::options::find options;
        options.cursor_type(mongocxx::cursor::type::k_tailable_await);

        mongocxx::cursor cursor = coll.find(filter.view(), options);

        while( _running )
        {
            if( cursor.begin() == cursor.end() )
            {
                std::unique_lock<mutex_t> guard(_mtx_stop);

                const bool finished = _cv.wait_for(guard,
                    std::chrono::milliseconds(_cfg._period),
                    [this](){ return !_running; });

                if( finished )
                    break;

                continue;
            }

            for( auto&& doc : cursor )
            {
                evtinfo_t info;
                bsoncxx::document::element e_ts = doc["ts"];
                if( !e_ts || e_ts.type() != bsoncxx::type::k_timestamp )
                    continue;

                const auto v_ts = e_ts.get_timestamp();
                info._ts = v_ts.timestamp;
                if( info._ts == 1 )
                    info._ts = v_ts.increment;

                if( info._ts < start_ts.timestamp )
                    continue;

                bsoncxx::document::element e_ns = doc["ns"];
                if( !e_ns || e_ns.type() != bsoncxx::type::k_utf8)
                    continue;

                info._ns = e_ns.get_utf8().value.to_string();

                bsoncxx::document::element e_op = doc["op"];
                if( !e_op || e_op.type() != bsoncxx::type::k_utf8)
                    continue;

                const std::string op = e_op.get_utf8().value.to_string();
                if( op == "i" )
                    info._evt = on_insert;
                else if( op == "u" )
                    info._evt = on_update;
                else if( op == "d" )
                    info._evt = on_remove;
                else if( op == "n" )
                    info._evt = on_noop;

                bsoncxx::document::element e_o = doc["o"];
                if( e_o && e_o.type() == bsoncxx::type::k_document )
                    info._o = e_o.get_document().view();

                bsoncxx::document::element e_o2 = doc["o2"];
                if( e_o2 && e_o2.type() == bsoncxx::type::k_document )
                    info._o2 = e_o2.get_document().view();

                filter_t f;
                f.first = op;
                run_callbacks(f, info); // запускаем для attach(op,"",...);

                f.second = info._ns;
                run_callbacks(f, info); // запускаем для attach(op,"ns",...);
            }
        }
    }

    void watcher::run_callbacks(const filter_t& f, evtinfo_t& info)
    {
        std::lock_guard<rmutex_t> guard(_mtx_modify);

        const auto it = _callbacks.find(f);

        if( it == _callbacks.end() )
            return;

        const callbacks_t::mapped_type& events = it->second;
        if( events.empty() )
            return;

        for(auto&& handler : it->second )
        {
            info._evtid = handler.first;
            handler.second(info);
        }
    }

    void watcher::stop()
    {
        std::unique_lock<mutex_t> lock(_mtx_stop);
        _running = false;
        lock.unlock();

        _cv.notify_one();
    }

}
