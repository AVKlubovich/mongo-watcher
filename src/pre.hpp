#ifndef __MONGO_WATCHER_PRE_HPP__
#define __MONGO_WATCHER_PRE_HPP__

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <string>
#include <unordered_map>

#endif // __MONGO_WATCHER_PRE_HPP__
