var classmongo_1_1watcher =
[
    [ "config_t", "d5/d8e/structmongo_1_1watcher_1_1config__t.html", "d5/d8e/structmongo_1_1watcher_1_1config__t" ],
    [ "evtinfo_t", "d3/d1b/structmongo_1_1watcher_1_1evtinfo__t.html", "d3/d1b/structmongo_1_1watcher_1_1evtinfo__t" ],
    [ "callback_t", "db/d9c/classmongo_1_1watcher.html#a630c13ab4e1885468eaa828dd102a15d", null ],
    [ "event_t", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669f", [
      [ "on_insert", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669fa3482cfacf1a4d20b4c2e7cdaef2b9125", null ],
      [ "on_update", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669faae1b2d67ec4606c16dfecbe43ea39cc2", null ],
      [ "on_remove", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669fa60089d0d1c18dc5c2b2acd2702da4acc", null ],
      [ "on_modify", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669fa3aff5f808329187a298933a49966c916", null ],
      [ "on_noop", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669fa8eca0f1cf91d430faa7b5bd51e2a30e0", null ],
      [ "on_any", "db/d9c/classmongo_1_1watcher.html#a1f42e83b65dd77ea166f2979aac5669fa2d2164ec3751adad3dd5cfb72b72a51d", null ]
    ] ],
    [ "watcher", "db/d9c/classmongo_1_1watcher.html#abeb915deb5151d8e5d6f33d0eacb7377", null ],
    [ "watcher", "db/d9c/classmongo_1_1watcher.html#ac72cfabd716af366dc499849fad926d7", null ],
    [ "~watcher", "db/d9c/classmongo_1_1watcher.html#ae7653bf786f549e23e7d221d576be647", null ],
    [ "attach", "db/d9c/classmongo_1_1watcher.html#a15027137bf94daea1d422bb99a452cc1", null ],
    [ "detach", "db/d9c/classmongo_1_1watcher.html#a76cc6cd9ecc91986e380252868240e6b", null ],
    [ "run", "db/d9c/classmongo_1_1watcher.html#a0a3a35449583b3b6193a114ef558da90", null ],
    [ "stop", "db/d9c/classmongo_1_1watcher.html#ab25547e18133442d3e1722a5ad19625c", null ]
];