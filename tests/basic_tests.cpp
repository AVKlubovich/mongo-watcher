#include "pre.hpp"
#include "doctest.h"

#include <mongo/watcher.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/exception/exception.hpp>
#include <bsoncxx/json.hpp>

TEST_CASE("watcher creation")
{
    mongo::watcher w;

    std::thread t([&w]()
    {
        try
        {
            w.run();
        }
        catch(std::exception& e)
        {
            std::cerr << "error: " << e.what() << std::endl;
        }
    });

    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    w.stop();
    t.join();
}

TEST_CASE("watch noop")
{
    mongo::watcher w;

    std::promise<mongo::watcher::evtinfo_t> promise;
    bool ready = false;
    const int evtid = w.attach(mongo::watcher::on_noop, "",
        [&promise, &ready](const mongo::watcher::evtinfo_t& info)
        {
            if( ready )
                return;

            promise.set_value(info);
            ready = true;
        }
    );

    std::thread t([&w]()
    {
        try
        {
            w.run(true);
        }
        catch(std::exception& e)
        {
            std::cerr << "error: " << e.what() << std::endl;
        }
    });

    try
    {
        auto f = promise.get_future();
        const auto status = f.wait_for(std::chrono::milliseconds(1000));

        w.detach(evtid);

        w.stop();
        t.join();

        REQUIRE(status == std::future_status::ready);

        const mongo::watcher::evtinfo_t info = f.get();
        CHECK(info._evtid == evtid);
        CHECK(info._ns.empty());
        CHECK(info._ts > 0);
        CHECK(info._evt == mongo::watcher::on_noop);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        w.stop();
        if( t.joinable() )
            t.join();
        throw;
    }
}

TEST_CASE("watch filtered")
{
    const std::time_t ts = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

    mongo::watcher w;

    std::promise<mongo::watcher::evtinfo_t> promise;
    bool ready = false;
    std::string json;
    const int evtid = w.attach(mongo::watcher::on_insert, "test.table",
        [&promise, &ready, &json](const mongo::watcher::evtinfo_t& info)
        {
            if( ready )
                return;

            json = bsoncxx::to_json(info._o);
            promise.set_value(info);
            ready = true;
        }
    );

    std::thread t([&w]()
    {
        try
        {
            w.run();
            std::cout << "test finished" << std::endl;
        }
        catch(std::exception& e)
        {
            std::cerr << "error: " << e.what() << std::endl;
        }
    });

    try
    {
        mongocxx::uri uri(mongocxx::uri::k_default_uri);
        mongocxx::client client(uri);
        mongocxx::database db = client["test"];
        mongocxx::collection c = db["table"];
        c.insert_one(
            bsoncxx::builder::stream::document{} <<
                "name" << "c++ developer" <<
                "age" << 39 <<
            bsoncxx::builder::stream::finalize
        );

        auto f = promise.get_future();
        const auto status = f.wait_for(std::chrono::milliseconds(1000));

        w.detach(evtid);

        w.stop();
        t.join();

        REQUIRE(status == std::future_status::ready);

        const mongo::watcher::evtinfo_t info = f.get();
        CHECK(info._evtid == evtid);
        CHECK(info._ns == "test.table");
        CHECK(info._ts >= ts);
        CHECK(info._evt == mongo::watcher::on_insert);
        CHECK(!json.empty());
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        w.stop();
        if( t.joinable() )
            t.join();
        throw;
    }
}
