#ifndef __MONGO_WATCHER_TESTS_PRE_HPP__
#define __MONGO_WATCHER_TESTS_PRE_HPP__

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <future>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>

#endif // __MONGO_WATCHER_TESTS_PRE_HPP__
