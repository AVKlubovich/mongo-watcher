ifeq ($(DBG),)
export hide=@
endif

export AR:=gcc-ar
export RANLIB:=gcc-ranlib
export OUTDIRNAME=_dist
export OUTDIR=../$(OUTDIRNAME)
export CXXFLAGS=-O2 -std=c++14 -DNDEBUG -ggdb -pthread -fexceptions -I../include \
	$(shell pkg-config --cflags libmongocxx) \
	$(shell pkg-config --cflags libbsoncxx)
export LDFLAGS=-rdynamic -pthread \
	$(shell pkg-config --libs libmongocxx) \
	$(shell pkg-config --libs libbsoncxx)

.PHONY: lib tests build clean clean-lib clean-tests run

build: lib tests

lib:
	@$(MAKE) --no-print-directory -C src

tests: lib
	@$(MAKE) --no-print-directory -C tests

clean: clean-lib clean-tests

clean-lib:
	@$(MAKE) --no-print-directory -C src clean

clean-tests:
	@$(MAKE) --no-print-directory -C tests clean

run: build
	cd $(OUTDIRNAME) && ./tests

lines:
	find $(SRC_DIRS) -name "*.cpp" -o -name "*.hpp" -a -not -name "doctest.h" | xargs cat | wc

docs:
	doxygen